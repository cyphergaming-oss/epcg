# Eclipse Phase Character Generator - Features Plan by Version

## v1.0

- Generate characters/NPCs according to Transhumanity's FATE character generator rules
- Support randomly generated characters only (initially)
- Create a "weighting" system to establish correlation between different backgrounds, morphs, motivations, etc. to skew generated characters towards plausible/viable builds
- Generate stunts and aspects with basic text generation
- Include only morphs, stunts, etc. from the Tranhumanity's FATE rulebook

## v1.1

- Store generated characters in database with UUID for later reference

## v2.0

- Add in basic character builder for non-generated characters
- Store custom-built characters in PC database with UUIDs and basic "owner manipulation string" (OMS) to allow people to update the characters.
  - Idea here is to avoid creating a whole login scheme and having to store credentials, so UUID + OMS allows you to save and modify the characters you create
- Add in more morphs, stunts, etc. that can be enabled optionally

## v3.0

- AI/ML integration for better text generation (think GPT3)?
- Use ML for NPC background text generation?
- Generate settlements of N number of NPCs, generating connections between them
- More morphs, stunts, etc.

## v4.0

- Story/adventure hook generation based on settlements and NPC interactions
- More morphs, stunts, etc.

## v5.0

- Add support for generating Eclipse Phase 2nd Edition NPCs

## v6.0

- Add support for creating Eclipse Phase 2nd Edition player characters
