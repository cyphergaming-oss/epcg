all: build docker

all-dev: build-dev docker

build:
	go build -ldflags="-s -w"
	upx --brute epcg

build-dev:
	go build

docker:
	docker build -t epcg .

clean:
	go clean
	bash ./run-local/cleanup.sh
