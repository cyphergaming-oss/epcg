package cmd

import (
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/cypher_zero/epcg/api_fate"
)

// startCmd represents the start command
var startCmd = &cobra.Command{
	Use:   "start",
	Short: "Starts EPCG API servers",
	Long:  `Starts EPCG API servers`,
	// Run: func(cmd *cobra.Command, args []string) {},
}

// startAllCmd represents the "start all" command
var startAllCmd = &cobra.Command{
	Use:   "all",
	Short: "Start all EPCG API servers",
	Long:  `Start all EPCG API servers`,
	Run: func(cmd *cobra.Command, args []string) {
		startAll()
	},
}

// starFateCmd represents the "start fate"
var startFateCmd = &cobra.Command{
	Use:   "fate",
	Short: "Start EPCG FATE API server",
	Long:  `Start EPCG FATE API server`,
	Run: func(cmd *cobra.Command, args []string) {
		startFateAPI()
	},
}

// Bool to init all databases before starting services
var initAllDBs bool

// Bool to init database before starting services
var initFateDB bool

func init() {
	rootCmd.AddCommand(startCmd)
	startCmd.AddCommand(startAllCmd)
	startCmd.AddCommand(startFateCmd)

	startCmd.PersistentFlags().BoolVar(
		&initAllDBs,
		"db-init-all",
		false,
		"Initialize all API databases before starting services",
	)

	startCmd.PersistentFlags().BoolVar(
		&initFateDB,
		"db-init-fate",
		false,
		"Initialize the FATE API database before starting service",
	)
}

// Sets all database init vars to true if initAllDBs is true
func setAllDBinits() {
	if initAllDBs {
		initFateDB = true
	}
}

// Start all EPCG API servers
func startAll() {
	startFateAPI()
}

// Start EPCG FATE API server
func startFateAPI() {
	setAllDBinits()

	if initFateDB {
		initDatabase(fateAPImap["data_path"].(string))
	}

	api_fate.FateDB = api_fate.ConnectFateDB(fatePsqlInfo)

	log.Println("Starting FATE handler")
	// TODO: Figure out how to launch in new thread
	api_fate.HandleRequests(fateAPImap["rest_port"].(int))
}
