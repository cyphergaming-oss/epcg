// File just for the main FATE API variable definitions used throughout the module

package cmd

// Path to config file
var cfgFile string

// Path to the FATE data with which to initilize the database
var fateDataPath string

// The URL of the database server to connect to for the FATE API
var fateDBhost string

// The port to connect to on the database for the FATE API
var fateDBport int

// The name of the database for the FATE API
var fateDBname string

// The database user to connect as for the FATE API
var fateDBuser string

// The password for the database user for the FATE API
var fateDBpass string

// The timezone of the database server for the FATE API
var fateDBtz string

// The port on which to expose the FATE API
var fateRestPort int

// Map of all the important variables for the FATE API; typically ingested from the config file
var fateAPImap map[string]interface{}

// PostGreSQL database connection info string
var fatePsqlInfo string

// PostGreSQL database SSL configuration
var fateDBssl string

// String slice of table names required by the FATE API.
// This is meant to be constant! Please don't mutate it except to sort.
var fateDBtablesList = []string{
	"aspect_adjectives",
	"aspect_nouns",
	"aspect_templates",
	"aspect_verbs",
	"backgrounds",
	"ego_stunts",
	"factions",
	"gear_stunts",
	"morph_stunts",
	"morph_traits",
	"morphs",
	"motivations",
	"skills",
	"team_roles",
}
