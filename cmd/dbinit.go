package cmd

import (
	"database/sql"
	"log"
	"sort"

	"github.com/spf13/cobra"
	"gitlab.com/cypher_zero/psql_csv"
)

// initCmd represents the init command
var dbInitCmd = &cobra.Command{
	Use:   "dbinit",
	Short: "Initializes the databases",
	Long:  `Initializes the databases required for the API. Only do this once`,
	Run: func(cmd *cobra.Command, args []string) {
		initDatabase(fateAPImap["data_path"].(string))
	},
}

func init() {
	rootCmd.AddCommand(dbInitCmd)
}

// Initializes the database for the FATE API from the CSV files at the provided path
func initDatabase(fateDBfilesPath string) {
	// PostGreSQL database connection
	var db *sql.DB = ConnectDatabase(fatePsqlInfo)
	// Slice of tables missing from the database
	var tablesMissing []string = checkTables(db)
	if len(tablesMissing) > 0 {
		log.Println("Missing tables:", tablesMissing)
		// Slice of CSV filenames to ingest into the database
		var fateCSVfiles []string = GetDBfiles(fateDBfilesPath)
		for i := range fateCSVfiles {
			psql_csv.CreateTableFromCSV(fateCSVfiles[i], db)
		}
		tablesMissing = checkTables(db)
		if len(tablesMissing) > 0 {
			log.Fatal("ERROR: Tables missing after ingest:", tablesMissing)
		}
	}
	log.Println("Database initialization complete.")
}

// Checks if all the required FATE API tables exist in the database and
// returns string slice of missing tables
func checkTables(db *sql.DB) []string {
	// The list of tables returned from the database
	var dbTables []string
	// The list of tables which are missing to return
	var tablesMissing []string
	var query string = `SELECT tablename FROM pg_catalog.pg_tables WHERE ` +
		`schemaname != 'pg_catalog' AND schemaname != 'information_schema';`

	log.Println("Getting all tables in the database.")
	res, err := db.Query(query)
	if err != nil {
		log.Println("DB query failed:\n", query)
		log.Fatal("DB error:", err)
	}

	if res != nil {
		for res.Next() {
			// Working table name to append to the list of DB tables
			var tablename string
			err := res.Scan(&tablename)
			if err != nil {
				log.Fatal("dbinit: Error scanning table names:", err)
			}
			dbTables = append(dbTables, tablename)
		}
		sort.Strings(dbTables)

		sort.Strings(fateDBtablesList)
		for i := range fateDBtablesList {
			if Contains(dbTables, fateDBtablesList[i]) {
				log.Println("Found table:" + fateDBtablesList[i])
			} else {
				log.Println("Table missing:" + fateDBtablesList[i])
				tablesMissing = append(tablesMissing, fateDBtablesList[i])
			}
		}
	}

	return tablesMissing
}
