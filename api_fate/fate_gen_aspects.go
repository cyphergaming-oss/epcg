package api_fate

import (
	"reflect"
	"regexp"
	"strings"

	rand "gitlab.com/cypher_zero/epcg/rnd"
)

// Generate high concept from faction, team role, and background
func genHighConcept(role Team_Role, fac Faction, bkg Background) string {
	// High concept string to return
	var high_concept string

	// Get a random adjective
	high_concept = getAspectAdj("any", "mental", "")

	// Generate concept from team role + background or faction
	if rand.Intn(2) == 0 {
		high_concept = high_concept + " " + bkg.Background + " " + role.Name
	} else {
		high_concept = high_concept + " " + fac.Faction + " " + role.Name
	}

	high_concept = strings.Title(high_concept)

	return high_concept
}

// Generate trouble
func genTrouble(faction string, bkg string, morph_name string) string {
	// Trouble to return
	var trouble string = fillTemplate(getTemplate(true, "ego"), true, faction, bkg, morph_name)

	return trouble
}

// Generate freeform aspect
func genFFaspect(faction string, bkg string, morph_name string) string {
	// Freeform aspect to return
	var ff_aspect string = fillTemplate(getTemplate(false, "ego"), false, faction, bkg, morph_name)
	return ff_aspect
}

// Generate morph aspect
func genMorphAspect(morph Morph, faction string, bkg string) string {
	// 5% chance to use one of the example aspects
	if rand.Intn(20) == 0 {
		// Slice of example aspects traits
		var exampleAspects []string = listFromMorphReflect(morph, "Sample_Aspect_")
		return exampleAspects[rand.Intn(len(exampleAspects))]

	} else {
		return fillTemplate(getTemplate(false, "morph"), false, faction, bkg, morph.Morph)
	}
}

// Retun a string slice of element names for the given interface
func listFromMorphReflect(reflectMe Morph, matchString string) []string {
	// Slice of strings to return
	var returnList []string
	// Reflect value of the provided interface
	var reflectElem reflect.Value = reflect.ValueOf(&reflectMe).Elem()

	for i := 0; i < reflectElem.NumField(); i++ {
		var varName string = reflectElem.Type().Field(i).Name
		var varType reflect.Type = reflectElem.Type().Field(i).Type

		if strings.Contains(varName, matchString) && varType.Name() == "string" {
			var varValue string = reflectElem.Field(i).Interface().(string)
			if varValue != "" {
				returnList = append(returnList, varValue)
			}
		}
	}
	return returnList
}

// Retun a map[string]int of element names and their integer values where the name matches
// the provided matchString for the given interface
func mapFromReflect(reflectElem reflect.Value, matchString string) map[string]int {
	// Map to return
	var returnMap map[string]int = make(map[string]int)

	for i := 0; i < reflectElem.NumField(); i++ {
		var varName string = reflectElem.Type().Field(i).Name
		var varType reflect.Type = reflectElem.Type().Field(i).Type

		if strings.Contains(varName, matchString) && varType.Name() == "int" {
			var varValue int = reflectElem.Field(i).Interface().(int)
			returnMap[varName] = varValue
		}
	}
	return returnMap
}

// Fill provided template string and return it
func fillTemplate(templ string, trouble bool, faction string, bkg string, morph_name string) string {
	// Regex to get elements that need to be replaced
	var re *regexp.Regexp = regexp.MustCompile(`{{[a-zA-Z0-9-_|'’ ]+}}`)
	// Slice of all elements to be replaced
	var orig_replace_elems_list []string = re.FindAllString(templ, -1)
	// Slice of new elements that will replace the original
	var new_replace_elems_list []string
	// Regex for nouns
	var noun_re *regexp.Regexp = regexp.MustCompile(`^noun`)
	// Regex for adjectives
	var adj_re *regexp.Regexp = regexp.MustCompile(`^adjective`)
	// Regex for list
	var list_re *regexp.Regexp = regexp.MustCompile(`^list`)
	// Regex for list
	var self_re *regexp.Regexp = regexp.MustCompile(`^self`)

	for _, rep_elem := range orig_replace_elems_list {
		// Strip culry braces
		rep_elem = strings.Trim(rep_elem, "{}")
		// String to check if a noun is required
		var noun string = noun_re.FindString(rep_elem)
		// String to check if an adjective is required
		var adj string = adj_re.FindString(rep_elem)
		// String to check if list
		var list_item string = list_re.FindString(rep_elem)
		// String to check if self
		var self string = self_re.FindString(rep_elem)

		rep_elem = strings.Replace(rep_elem, "noun", "", 1)
		rep_elem = strings.Replace(rep_elem, "adjective", "", 1)
		rep_elem = strings.Replace(rep_elem, "list", "", 1)
		rep_elem = strings.Replace(rep_elem, "self", "", 1)
		rep_elem = strings.TrimPrefix(rep_elem, "-")

		// List of component requirements
		var elems []string = strings.Split(rep_elem, "-")

		// The type of noun to get
		var elem_type string = ""
		// The sub-type of the nount to get
		var elem_subtype string = ""

		if noun != "" {
			// Whether the noun should be plural
			var plural bool = false

			if len(elems) > 0 {
				for i, elem := range elems {
					if elem == "plural" {
						plural = true
					} else if i == 0 {
						elem_type = elem
					} else if i == 1 {
						elem_subtype = elem
					}
				}
			}
			rep_elem = getAspectNoun(elem_type, elem_subtype, plural)

		} else if adj != "" {
			// Whether the adjective should be, positive, negative, or any
			var adj_quality string = "any"
			if len(elems) > 0 {
				for i, elem := range elems {
					if i == 0 {
						adj_quality = elem
					} else if i == 1 {
						elem_type = elem
					} else if i == 2 {
						elem_subtype = elem
					}
				}
			}

			rep_elem = getAspectAdj(adj_quality, elem_type, elem_subtype)

		} else if list_item != "" {
			elems = strings.Split(rep_elem, "|")
			random_nubmer := rand.Intn(len(elems))
			rep_elem = elems[random_nubmer]

		} else if self != "" {
			if rep_elem == "faction" {
				rep_elem = strings.ToLower(faction)
			} else if rep_elem == "background" {
				rep_elem = strings.ToLower(bkg)
			} else if rep_elem == "morph" {
				rep_elem = strings.ToLower(morph_name)
			}
		}

		new_replace_elems_list = append(new_replace_elems_list, rep_elem)
	}

	for i, rep_elem := range orig_replace_elems_list {
		templ = strings.Replace(templ, rep_elem, new_replace_elems_list[i], 1)
	}

	return templ
}
