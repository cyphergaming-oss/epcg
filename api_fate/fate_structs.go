package api_fate

import (
	"reflect"

	"gorm.io/gorm"
)

type Fate_Char struct {
	Name        string            `json:"name"`
	Ego_Aspects map[string]string `json:"ego_aspects"`
	Fate_Points int               `json:"fate_points"`
	Refresh     int               `json:"refresh"`
	Gender      string            `json:"ego_gender"`
	Background  string            `json:"background"`
	Faction     string            `json:"faction"`
	Motivations []string          `json:"motivations"`
	Skills      map[string]int    `json:"skills"`
	Ego_Stunts  []Stunt_Out       `json:"ego_stunts"`
	Morphs      []Morph_Out       `json:"morphs"`
	Muse        Muse_Out          `json:"muse"`
	Gear        []Gear            `json:"gear"`
}

type Morph_Out struct {
	Morph        string      `json:"morph"`
	Type         string      `json:"type"`
	Refresh_Cost int         `json:"refresh_cost"`
	Durability   string      `json:"durability"`
	Sex          string      `json:"sex"`
	Aspects      []string    `json:"aspects"`
	Traits       []string    `json:"traits"`
	Stunts       []Stunt_Out `json:"stunts"`
}

type Gear struct {
	Name string      `json:"name"`
	Info interface{} `json:"info"`
}

type Stunt_Out struct {
	Stunt         string `json:"stunt"`
	Description   string `json:"description"`
	Related_Skill string `json:"related_skill"`
	Use_Cost      int    `json:"use_cost"`
}

type Muse_Out struct {
	Name    string         `json:"name"`
	Skills  map[string]int `json:"skills"`
	Details interface{}    `json:"details"`
}

// Gorm structs

//lint:ignore U1000 - false positive
type mapMorphWeights interface {
	morphWeightsMap() map[string]int
}

type Team_Role struct {
	gorm.Model
	Name                    string
	Role_Weight             int
	Peak_Skill              string
	Key_Skill_1             string
	Key_Skill_2             string
	Key_Skill_3             string
	Bouncer_Morph_Weight    int
	Flat_Morph_Weight       int
	Fury_Morph_Weight       int
	Ghost_Morph_Weight      int
	Menton_Morph_Weight     int
	Neo__Avian_Morph_Weight int
	Neo__Pig_Morph_Weight   int
	Neotenic_Morph_Weight   int
	Octomorph_Morph_Weight  int
	Ruster_Morph_Weight     int
	Sylph_Morph_Weight      int
	Splicer_Morph_Weight    int
	Novacrab_Morph_Weight   int
	Scurrier_Morph_Weight   int
	Worker_Pod_Morph_Weight int
	Case_Morph_Weight       int
	Flexbot_Morph_Weight    int
	Opteryx_Morph_Weight    int
	Reaper_Morph_Weight     int
	Swarmanoid_Morph_Weight int
	Synth_Morph_Weight      int
	Eidolon_Morph_Weight    int
	Infomorph_Morph_Weight  int
}

func (m Team_Role) morphWeightsMap() map[string]int {
	// Reflect value of the provided Team_Role
	var reflectElem reflect.Value = reflect.ValueOf(&m).Elem()

	return mapFromReflect(reflectElem, "_Morph_Weight")
}

type Background struct {
	gorm.Model
	Background                string
	Description               string
	Background_Gen_Weight     int
	Anarchist_Faction_Weight  int
	Argonaut_Faction_Weight   int
	Barsoomian_Faction_Weight int
	Brinker_Faction_Weight    int
	Criminal_Faction_Weight   int
	Extropian_Faction_Weight  int
	Hypercorp_Faction_Weight  int
	Jovian_Faction_Weight     int
	Lunar_Faction_Weight      int
	Mercurial_Faction_Weight  int
	Scum_Faction_Weight       int
	Socialite_Faction_Weight  int
	Titanian_Faction_Weight   int
	Ultimate_Faction_Weight   int
	Venusian_Faction_Weight   int
	Bouncer_Morph_Weight      int
	Flat_Morph_Weight         int
	Fury_Morph_Weight         int
	Ghost_Morph_Weight        int
	Menton_Morph_Weight       int
	Neo__Avian_Morph_Weight   int
	Neo__Pig_Morph_Weight     int
	Neotenic_Morph_Weight     int
	Octomorph_Morph_Weight    int
	Ruster_Morph_Weight       int
	Sylph_Morph_Weight        int
	Splicer_Morph_Weight      int
	Novacrab_Morph_Weight     int
	Scurrier_Morph_Weight     int
	Worker_Pod_Morph_Weight   int
	Case_Morph_Weight         int
	Flexbot_Morph_Weight      int
	Opteryx_Morph_Weight      int
	Reaper_Morph_Weight       int
	Swarmanoid_Morph_Weight   int
	Synth_Morph_Weight        int
	Eidolon_Morph_Weight      int
	Infomorph_Morph_Weight    int
}

func (m Background) morphWeightsMap() map[string]int {
	// Reflect value of the provided Background
	var reflectElem reflect.Value = reflect.ValueOf(&m).Elem()

	return mapFromReflect(reflectElem, "_Morph_Weight")
}

type Faction struct {
	gorm.Model
	Faction                 string
	Description             string
	Bouncer_Morph_Weight    int
	Flat_Morph_Weight       int
	Fury_Morph_Weight       int
	Ghost_Morph_Weight      int
	Menton_Morph_Weight     int
	Neo__Avian_Morph_Weight int
	Neo__Pig_Morph_Weight   int
	Neotenic_Morph_Weight   int
	Octomorph_Morph_Weight  int
	Ruster_Morph_Weight     int
	Sylph_Morph_Weight      int
	Splicer_Morph_Weight    int
	Novacrab_Morph_Weight   int
	Scurrier_Morph_Weight   int
	Worker_Pod_Morph_Weight int
	Case_Morph_Weight       int
	Flexbot_Morph_Weight    int
	Opteryx_Morph_Weight    int
	Reaper_Morph_Weight     int
	Swarmanoid_Morph_Weight int
	Synth_Morph_Weight      int
	Eidolon_Morph_Weight    int
	Infomorph_Morph_Weight  int
}

func (m Faction) morphWeightsMap() map[string]int {
	// Reflect value of the provided Faction
	var reflectElem reflect.Value = reflect.ValueOf(&m).Elem()

	return mapFromReflect(reflectElem, "_Morph_Weight")
}

type Motivation struct {
	gorm.Model
	Motivation string
	Opposites  string
	//lint:ignore U1000 - false positive
	Drifter_bkg_weight int
	//lint:ignore U1000 - false positive
	Fall_evacuee_bkg_weight int
	//lint:ignore U1000 - false positive
	Hyperelite_bkg_weight int
	//lint:ignore U1000 - false positive
	Infolife_bkg_weight int
	//lint:ignore U1000 - false positive
	Isolate_bkg_weight int
	//lint:ignore U1000 - false positive
	Lost_bkg_weight int
	//lint:ignore U1000 - false positive
	Lunar_colonist_bkg_weight int
	//lint:ignore U1000 - false positive
	Martian_bkg_weight int
	//lint:ignore U1000 - false positive
	Mriginal_space_colonist_bkg_weight int
	//lint:ignore U1000 - false positive
	Re__instantiated_bkg_weight int
	//lint:ignore U1000 - false positive
	Scumborn_bkg_weight int
	//lint:ignore U1000 - false positive
	Uplift_bkg_weight int
	//lint:ignore U1000 - false positive
	Anarchist_faction_weight int
	//lint:ignore U1000 - false positive
	Argonaut_faction_weight int
	//lint:ignore U1000 - false positive
	Barsoomian_faction_weight int
	//lint:ignore U1000 - false positive
	Brinker_faction_weight int
	//lint:ignore U1000 - false positive
	Criminal_faction_weight int
	//lint:ignore U1000 - false positive
	Extropian_faction_weight int
	//lint:ignore U1000 - false positive
	Hypercorp_faction_weight int
	//lint:ignore U1000 - false positive
	Jovian_faction_weight int
	//lint:ignore U1000 - false positive
	Lunar_faction_weight int
	//lint:ignore U1000 - false positive
	Mercurial_faction_weight int
	//lint:ignore U1000 - false positive
	Scum_faction_weight int
	//lint:ignore U1000 - false positive
	Socialite_faction_weight int
	//lint:ignore U1000 - false positive
	Titanian_faction_weight int
	//lint:ignore U1000 - false positive
	Ultimate_faction_weight int
	//lint:ignore U1000 - false positive
	Venusian_faction_weight int
}

type Morph struct {
	gorm.Model
	Morph                    string
	Type                     string
	Sub_type                 string
	Description              string
	Durability               string
	Refresh_Cost             int
	Cred_Cost                string
	Sample_Aspect_1          string
	Sample_Aspect_2          string
	Sample_Aspect_3          string
	Sample_Aspect_4          string
	Trait_1                  string
	Trait_2                  string
	Trait_3                  string
	Trait_4                  string
	Trait_5                  string
	Trait_6                  string
	Trait_7                  string
	Trait_8                  string
	Stunt_1                  string
	Stunt_2                  string
	Stunt_3                  string
	Stunt_4                  string
	Stunt_5                  string
	Stunt_6                  string
	Stunt_7                  string
	Stunt_8                  string
	Sex_Weight_Male          int
	Sex_Weight_Female        int
	Sex_Weight_Axexual       int
	Sex_Weight_Hermaphrodite int
}

type Skill struct {
	gorm.Model
	Skill      string
	AI_Valid   bool
	Muse_Valid bool
}

type Morph_Stunt struct {
	gorm.Model
	Stunt            string
	Associated_Skill string
	Description      string
	Use_Cost         int
	Restrictions     string
}

type Ego_Stunt struct {
	gorm.Model
	Stunt            string
	Associated_Skill string
	Description      string
	Use_Cost         int
	Restrictions     string
}

type Gear_Stunt struct {
	gorm.Model
	Stunt            string
	Associated_Skill string
	Description      string
	Use_Cost         int
	Restrictions     string
}

type aspect_noun struct {
	gorm.Model
	Noun                string
	Plural              string
	Type                string
	Subtype             string
	Related_Morphs      string
	Related_Backgrounds string
	Related_Factions    string
}

type aspect_verb struct {
	gorm.Model
	Verb                string
	Type                string
	Subtype             string
	Related_Morphs      string
	Related_Backgrounds string
	Related_Factions    string
	Trouble             bool
}

type aspect_adjective struct {
	gorm.Model
	Adjective           string
	Quality             string
	Type                string
	Subtype             string
	Morph_Types         string
	Related_Backgrounds string
	Related_Factions    string
}

type aspect_template struct {
	gorm.Model
	Template string
	Type     string
	Subtype  string
	Trouble  bool
}
