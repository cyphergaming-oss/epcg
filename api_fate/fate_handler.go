package api_fate

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

func DisplayExampleChar(w http.ResponseWriter, r *http.Request) {
	log.Println("Endpoint hit: DisplayExampleChar")
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	_, err := w.Write(ExampleChar())
	if err != nil {
		log.Fatal("FATE handler write error:", err)
	}
}

func DisplayGeneratedCharacter(w http.ResponseWriter, r *http.Request) {
	log.Println("Endpoint hit: DisplayGeneratedCharacter")
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	_, err := w.Write(GenerateCharacter())
	if err != nil {
		log.Fatal("FATE handler write error:", err)
	}
}

func newREST() *mux.Router {
	r := mux.NewRouter()
	r.HandleFunc("/fate/generate", DisplayGeneratedCharacter)
	r.HandleFunc("/fate/example", DisplayExampleChar)
	return r
}

func HandleRequests(port int) {
	router := newREST()
	credentials := handlers.AllowCredentials()
	methods := handlers.AllowedMethods([]string{"GET"})
	// ttl := handlers.MaxAge(3600)
	origins := handlers.AllowedOrigins([]string{"*"})
	log.Fatal(http.ListenAndServe(":"+fmt.Sprint(port), handlers.CORS(credentials, methods, origins)(router)))
}
