package api_fate

import (
	"encoding/json"
	"log"
	"strings"

	rand "gitlab.com/cypher_zero/epcg/rnd"
)

func GenerateCharacter() []byte {
	// Generated fate character to be returned
	var fchar Fate_Char
	// First randomly selected morph - output struct
	var morph_out1 Morph_Out
	// First randomly selected morph
	var morph1 Morph
	// Fate points/Refresh the character has
	var fate_points int = 6
	// List of gear stunts
	var gear_stunts []Stunt_Out
	// Map of ego aspects
	var ego_aspects map[string]string = make(map[string]string)

	// Randomly chose team role to use as basis for character concept
	var role Team_Role = getWeightedTeamRole()

	// Backgound
	var bkg Background = genBackground()
	fchar.Background = bkg.Background

	// Faction name
	fchar.Faction = genFactionFromBackground(bkg)
	// Faction
	var faction = getFaction(fchar.Faction)

	// Motivations
	fchar.Motivations = genMotivations(fchar.Background, fchar.Faction)

	// First morph generation
	morph_out1.Morph = genMorph(role, bkg, faction)

	// Get morph stats from DB
	morph1 = getMorph(morph_out1.Morph)

	// Deduct cost of morph from refresh
	fate_points = fate_points - morph1.Refresh_Cost

	// Assign morph stats to morph_out1 output struct
	morph_out1.Type = morph1.Type
	morph_out1.Refresh_Cost = morph1.Refresh_Cost
	morph_out1.Durability = morph1.Durability
	morph_out1.Sex = genMorphSex(morph1)
	morph_out1.Traits = getMorphTraits(morph1)
	morph_out1.Stunts = genMorphStunts(morph1, fate_points-2)

	fate_points = fate_points - len(morph_out1.Stunts)

	// Generate ego aspects
	ego_aspects["High Concept"] = genHighConcept(role, faction, bkg)
	ego_aspects["Trouble"] = genTrouble(fchar.Faction, fchar.Background, morph1.Morph)
	ego_aspects["Freeform 1"] = genFFaspect(fchar.Faction, fchar.Background, morph1.Morph)
	ego_aspects["Freeform 2"] = genFFaspect(fchar.Faction, fchar.Background, morph1.Morph)

	// Generate morph aspect
	morph_out1.Aspects = append(morph_out1.Aspects, genMorphAspect(morph1, fchar.Faction, fchar.Background))
	// Generate a second morph aspect 70% of the time
	if rand.Intn(10) <= 6 {
		morph_out1.Aspects = append(morph_out1.Aspects, genMorphAspect(morph1, fchar.Faction, fchar.Background))
	}

	// Generate ego gender
	fchar.Gender = genEgoGender(morph_out1, fchar.Background)

	// Add morph_out1 to output struct
	fchar.Morphs = append(fchar.Morphs, morph_out1)

	// Generate skills
	fchar.Skills = genSkills(role, morph1.Type)

	// Generate two ego stunts
	fchar.Ego_Stunts = append(fchar.Ego_Stunts, genEgoStunt(fchar.Skills, fchar.Ego_Stunts))
	fchar.Ego_Stunts = append(fchar.Ego_Stunts, genEgoStunt(fchar.Skills, fchar.Ego_Stunts))
	fate_points = fate_points - 2

	// Generate more stunts
	more_stunts := true
	for more_stunts {
		// Don't add more stunts if character only has 1 fate point
		if fate_points == 1 {
			more_stunts = false
		} else {
			// Randomly determine if a stunt should be added. The more FATE points a character still has,
			// the more likely it is that they'll get a new stunt.
			random_number := rand.Intn(fate_points)
			if random_number > 0 {
				// 50-50 chance of getting a Gear stunt if not an infomorph
				random_number = rand.Intn(2)
				if random_number == 1 && strings.Trim(strings.ToLower(morph1.Type), `\s`) != "infomorph" {
					gear_stunt := genGearStunt(fchar.Skills, gear_stunts)
					fchar.Gear = append(fchar.Gear, Gear{Name: gear_stunt.Stunt, Info: gear_stunt})
				} else {
					fchar.Ego_Stunts = append(fchar.Ego_Stunts, genEgoStunt(fchar.Skills, fchar.Ego_Stunts))
				}
				fate_points = fate_points - 1
			} else {
				more_stunts = false
			}
		}
	}

	fchar.Muse.Skills = genMuseSkills()
	fchar.Ego_Aspects = ego_aspects
	fchar.Fate_Points = fate_points
	fchar.Refresh = fate_points
	fchar_json, err := json.MarshalIndent(fchar, "", "  ")
	if err != nil {
		log.Fatal("Fatal error marshalling JSON: ", err)
	}

	return fchar_json
}

// Return random selection from map of weighted strings
func calcFromWeights(weight_map map[string]int) string {
	// Randomly selected string to return
	var output string
	// Map of the computed upper and lower bound weights
	var weight_sum_map = make(map[string][]int)
	// Sum of all weights in the provided map
	var weights_sum = 0

	// Calculate the map of valid upper and lower values for each entry
	for name, weight := range weight_map {
		weight_sum_map[name] = append(weight_sum_map[name], weights_sum+1)
		weights_sum = weights_sum + weight
		weight_sum_map[name] = append(weight_sum_map[name], weights_sum)
	}

	// Random number used to select entry
	var random_number int = rand.Intn(weights_sum) + 1

	// Find the entry that corresponds to the random number
	for name, k := range weight_sum_map {
		if random_number >= k[0] && random_number <= k[1] {
			output = name
		}
	}

	return output
}
