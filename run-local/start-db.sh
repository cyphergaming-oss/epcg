#!/bin/bash

# Script: run-local.sh
#
# Script to run the EPCG project on the local system, using Docker containers for non-EPCG code.
# Intended only for local testing; *not* recommended for production use.

set -e

# Start Postgres container
{
  docker run -d --name epcg-postgres \
    -e POSTGRES_PASSWORD=password \
    -e POSTGRES_USER=epcg \
    -e POSTGRES_DB=epcg_fate \
    -p 5432:5432 \
    postgres
} || {
  docker start epcg-postgres
}

docker ps
