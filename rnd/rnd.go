// Ref: https://yourbasic.org/golang/crypto-rand-int/

package rnd

import (
	crand "crypto/rand"
	"math/big"
)

// Returns a truly random number 0 to i-1
func Intn(i int) int {
	nBig, err := crand.Int(crand.Reader, big.NewInt(int64(i)))
	if err != nil {
		panic(err)
	}

	return int(nBig.Int64())
}
